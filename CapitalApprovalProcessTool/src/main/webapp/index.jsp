<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<body>
	<h2>Hello World!</h2>
	<form:errors path="user.*" />
	<form action="/CapitalApprovalProcessTool/login">
		Username: <input type="text" id="username" name="userName" /> <br>
		First Name: <input type="text" id="firstName" name="firstName" /> <br>
		<input type="submit" value="Go To Users' Page" />
	</form>
</body>
</html>
