<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Users</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
	</head>
	<body onload="load();">
		<form:errors path="user.*" />
		<form action="/CapitalApprovalProcessTool/capitalrequest" method="post">
			<input type="hidden" id="user_id"> 
			Name: <input type="text" id="name" name="userName"> <br> 
			Email: <input type="email" id="email" name="email"> <br> 
			Contact Number: <input type="text" id="contactNumber" required="required" name="contactNumber"> <br>
			<!-- 		<button onclick="submit();">Submit</button> -->
			<!-- 	<select name="cars" multiple="multiple">
				<option>Porsche Cayenne GTS 2028</option>
				<option>Honda Civic 1999</option>
				<option>Mazda MX-5 2021</option>
				<option>Subaru Levorg</option>
			</select> -->
			<input type="submit" value="Go to Capital Request Page" />
			<table id="table" border=1>
				<tr>
					<th>Name</th>
					<th>Email</th>
					<th>Edit</th>
					<th>Delete</th>
				</tr>
			</table>
	
			<script type="text/javascript">
				data = "";
				submit = function() {
	
					$.ajax({
						url : 'saveOrUpdate',
						type : 'POST',
						data : {
							id : $("#user_id").val(),
							userName : $('#name').val(),
							email : $('#email').val()
						},
						success : function(response) {
							alert(response.message);
							load();
						}
					});
					document.getElementById('name').value = '';
					document.getElementById('email').value = '';
				}
	
				delete_ = function(id) {
					alert(id);
					$.ajax({
						url : 'delete',
						type : 'POST',
						data : {
							userid : $("#user_id").val()
						},
						success : function(response) {
							alert(response.message);
							load();
						}
					});
				}
	
				edit = function(index) {
					$("#user_id").val(data[index].id);
					alert(data[index].id);
					$("#name").val(data[index].userName);
					alert(data[index].userName);
					$("#email").val(data[index].email);
	
				}
	
				load = function() {
					$.ajax({
						url : 'list',
						type : 'POST',
						success : function(response) {
							data = response.data;
							$('.tr').remove();
							for (i = 0; i < response.data.length; i++) {
								$("#table")
									.append(
										"<tr class='tr'> <td> "
										+ response.data[i].userName
										+ " </td> <td> "
										+ response.data[i].email
										+ " </td> <td> <a href='#' onclick= edit("
										+ i
										+ ");> Edit </a>  </td> </td> <td> <a href='#' onclick='delete_("
										+ response.data[i].id
										+ ");'> Delete </a>  </td> </tr>");
							}
						}
					});
				}
			</script>
		</form>
	</body>
</html>