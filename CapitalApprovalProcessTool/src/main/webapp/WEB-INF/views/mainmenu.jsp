<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
		<title>Insert title here</title>
	</head>
	<body>
		<form action="/CapitalApprovalProcessTool/mainmenu" method="post">
			${welcomeMsg} <br>
			<input type="submit" value="Create New Request" name="targetPage" />
			<input type="submit" value="View Request History" name="targetPage" />
			<input type="submit" value="View Administration" name="targetPage" />
		</form>
	</body>
</html>