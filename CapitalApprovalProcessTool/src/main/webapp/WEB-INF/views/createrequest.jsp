<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="/CapitalApprovalProcessTool/css/capitalrequest.css">
<title>Insert title here</title>
<script>
	function viewTab(evt, tabId) {
		var i, tabcontent, tablinks;
		tabcontent = document.getElementsByClassName("tabcontent");
		for (i = 0; i < tabcontent.length; i++) {
			tabcontent[i].style.display = "none";
		}
		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className
					.replace(" active", "");
		}
		document.getElementById(tabId).style.display = "block";
		evt.currentTarget.className += " active";
		evt.preventDefault();
	}
</script>

</head>
<body>
	<form:errors />
	<form action="/CapitalApprovalProcessTool/createrequest" method="post">

		<div id="requestDetails">
			<div id="requestTabs" class="tab">
				<button class="tablinks" onclick="viewTab(event, 'main')">Main</button>
				<button class="tablinks" onclick="viewTab(event, 'equipment')">Equipment</button>
				<button class="tablinks" onclick="viewTab(event, 'justification')">Justification</button>
				<button class="tablinks" onclick="viewTab(event, 'attachments')">Attachments</button>
				<button class="tablinks" onclick="viewTab(event, 'approvals')">Approvals</button>
			</div>

			<div id="main" class="tabcontent">
				<label>Requester's Name</label> <input type="text" name="submitter"/> <br>
				<label>Submitter's Name</label> <input type="text" name="requested"/> <br>
				<label>Description of Capital Being Requested</label> <input type="text" name="descriptionOfCapitalReq"/> <br>
				<label>Request Type</label> <input type="text" name="type"/> <br>
				<label>Was this forecasted for:</label> <input type="radio" name="isForecasted"/> <br>
				<label>Quarter and Year Needed:</label> <input type="text" name="qtrYearNeeded"/> <br>
				<label>Authorization Quarter and Year:</label> <input type="text" name="qtrYearAuth"/> <br>
				<label>Site Location</label> <input type="text" name="siteLocation"/> <br>
			</div> 
	
			<div id="equipment" class="tabcontent">
				<table border="1">
					<tr>
						<td>Equipment Type</td>
						<td>Description</td>
						<td>Unit Cost in USD</td>
						<td>Quantity</td>
						<td>Total Cost</td>
					</tr>
					<tr>
						<td><input type="text"/></td>
						<td><input type="text"/></td>
						<td><input type="text"/></td>
						<td><input type="text"/></td>
						<td><input type="text" readonly="readonly"/></td>
					</tr>
					<tr>
						<td><input type="text"/></td>
						<td><input type="text"/></td>
						<td><input type="text"/></td>
						<td><input type="text"/></td>
						<td><input type="text" readonly="readonly"/></td>
					</tr>
					<tr>
						<td><input type="text"/></td>
						<td><input type="text"/></td>
						<td><input type="text"/></td>
						<td><input type="text"/></td>
						<td><input type="text" readonly="readonly"/></td>
					</tr>
					<tr>
						<td><input type="text"/></td>
						<td><input type="text"/></td>
						<td><input type="text"/></td>
						<td><input type="text"/></td>
						<td><input type="text" readonly="readonly"/></td>
					</tr>
				</table>
			</div>
	
			<div id="justification" class="tabcontent">
				<br><label><strong>Justification Question 1:</strong> Please explain/justify in detail why this capital investment should be made (who what when why)? Including the comments describing the background</label> 
				    <input type="text" name="answer1"/>
				<br><label><strong>Justification Question 2:</strong> Is this New Capital Request? (Y/N)</label> 
				    <input type="text" name="answer2"/>
				<br><label><strong>Justification Question 3:</strong> Is the request part of capital previously approved and stopped in Bond? If yes, indicate when capital was approved, amount of total capital that was approved and how much capital has already been processed.</label> <input type="text" name="answer3"/>
				<br><label><strong>Justification Question 4:</strong> Contractual Obligation? (Y/N)</label> 
				    <label>Yes</label><input type="radio" name="answer4" value="Yes"/>
				    <label>No</label><input type="radio" name="answer4" value="No"/>
				<br><label><strong>Justification Question 5:</strong> Billed to Client Y/N - if yes explain how client is billed. Will the capital be billed out all at once, or amortized? Was the capital included in the original case?</label> 
				    <input type="text" name="answer5"/>
				<br><label><strong>Justification Question 6:</strong> Provide the linkage between the equipment (IBM SAN and OEM Servers) and increased billings and/or contractual commitments.</label> 
				    <input type="text" name="answer6"/>
				<br><label><strong>Justification Question 7:</strong> Can this be pushed to the next quarter? Why not? (Include Key Delivery Date) </label> 
				    <input type="text" name="answer7"/>
				<br><label><strong>Justification Question 8:</strong> Can another solution be implemented? Why not?</label> 
				    <input type="text" name="answer8"/>
				<br><label><strong>Justification Question 9:</strong> Can we split the capital expense? Why not? (X% current quarter, remainder next quarter)</label> 
				     <input type="text" name="answer9"/>
			</div>
			
			<div id="attachments" class="tabcontent">
				<label>Capital Approval Template</label>
				<br><label>Attachment 1</label><input type="file" />
				<br><label>Attachment 2</label><input type="file" />
				<br><label>Attachment 3</label><input type="file" />
				<br><label>Attachment 4</label><input type="file" />
				<br><label>Attachment 5</label><input type="file" />
				<br><label>DPE/Manager's Approval</label> 
				<label>PDF Attachment</label><input type="file" />	
			</div>
			
			<div id="approvals" class="tabcontent">
			</div>			
		</div>							
		
	</form>
</body>
</html>