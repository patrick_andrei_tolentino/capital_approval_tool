package com.ibm.capitalapprovaltool.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

public class HibernateFactoryUtil {
	private static SessionFactory sessionFactory = new AnnotationConfiguration().configure().buildSessionFactory();
	private static Session session;
	
	public static SessionFactory sessionFactory() {
		return sessionFactory;
	}
	
	public static Session getSession() {
		if(session == null || !session.isOpen()) {
			session = sessionFactory.openSession();
			session.beginTransaction();
		}
		return session;
	}
	
	public static void commitTransaction() {
		session.getTransaction().commit();
		session.close();
	}
	
}
