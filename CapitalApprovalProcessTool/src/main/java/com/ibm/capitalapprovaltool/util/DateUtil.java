package com.ibm.capitalapprovaltool.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtil {
	public static final SimpleDateFormat dotted_MMddyyyy = new SimpleDateFormat("MM.dd.yyyy");
	public static final SimpleDateFormat fullDotted_MMddYYYY_HHmm = new SimpleDateFormat("MM.dd.yyyy HH:mm");
	public static final SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyyMMdd");
	public static final SimpleDateFormat dashed_yyyyMmdd = new SimpleDateFormat("yyyy-MM-dd");
	public static final SimpleDateFormat slashed_MMddyyyy = new SimpleDateFormat("MM/dd/yyyy");
	public static final SimpleDateFormat default_fullDate = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");

	public static Date getDotted_MMddyyyy_DateObject(String formattedDate) {
		return formatStringToDateObject(formattedDate, dotted_MMddyyyy);
	}

	public static Date formatStringToDateObject(String formattedDate, SimpleDateFormat dateformat) {
		if (formattedDate.isEmpty()) {
			return null;
		}

		try {
			Date objDate = dateformat.parse(formattedDate);
			return objDate;
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}

	public static String toDotted_MMddyyyy_StringDate(Date date) {
		return toFormattedStringDate(date, dotted_MMddyyyy);
	}

	
	public static String toFormattedStringDate(Date date, SimpleDateFormat dateformat) {
		if (date == null) {
			return "";
		}
		return dateformat.format(date);
	}

	public static String toDotted_Full_DateFormat(Date date) {
		if (date == null) {
			return "";
		}
		return fullDotted_MMddYYYY_HHmm.format(date);
	}

	public static Date stringToDateObject(String dateString) {
		if (dateString == null || dateString.isEmpty()) {
			return null;
		}

		try {
			String pattern = findDatePattern(dateString);
			SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
			Date date = dateFormat.parse(dateString);
			return date;
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}


	private static String findDatePattern(String strDate) {
		String[] aa = strDate.split("/");
		if (aa.length == 3)
			return "MM/dd/yyyy";

		aa = strDate.split("-");
		if (aa.length == 3) {
			if (aa[1].length() > 2)
				return "dd-MMM-yyyy";
			else
				return "MM-dd-yyyy";
		}

		String dot = "\\.";
		aa = strDate.split(dot);
		if (aa.length == 3)
			return "MM.dd.yyyy";

		return "";
	}

	public static String getDay(Date d) {
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(d);
		return String.valueOf(gc.get(Calendar.DATE));
	}

	public static String getMonth(Date d) {
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(d);
		return String.valueOf(gc.get(Calendar.MONTH));
	}

	public static String getYear(Date d) {
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(d);
		return String.valueOf(gc.get(Calendar.YEAR));
	}
}
