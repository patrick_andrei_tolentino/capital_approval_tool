package com.ibm.capitalapprovaltool.main;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.omg.CORBA.Request;

import com.ibm.capitalapprovaltool.entity.CapitalAttachment;
import com.ibm.capitalapprovaltool.entity.CapitalEquipment;
import com.ibm.capitalapprovaltool.entity.CapitalRequest;
import com.ibm.capitalapprovaltool.enums.Organization;
import com.ibm.capitalapprovaltool.enums.QuarterYear;
import com.ibm.capitalapprovaltool.enums.RequestType;
import com.ibm.capitalapprovaltool.util.HibernateFactoryUtil;

public class Main {
	
	public static void main(String[] args) {
		Session session = HibernateFactoryUtil.getSession();
		
		CapitalRequest request = new CapitalRequest();
		request.setMasterDocNum("master doc 001");
		request.setType(RequestType.FACILITIES.getStringValue());
		request.setOrganization(Organization.GBS.toString());
		request.setCategory("sample category");
		request.setStatus("Pending for Approval");
		request.setDateCreated(new Date());
		request.setDateSubmitted(new Date());
		request.setContractName("Patrick's 5,000,000 USD contract");
		request.setProjectName("Capital Approval Process Tool");
		request.setSubmitter("Patrick Andrei V. Tolentino");
		request.setRequester("Christine Tayson");
		request.setForecasted(true);
		request.setQtrYearNeeded(QuarterYear.Q1_2018.getStringValue());
		request.setQtrYearAuth(QuarterYear.Q3_2018.getStringValue());
		request.setSiteLocation("Philippines");
		request.setAnswer1("answer1");
		request.setAnswer2("answer2");
		request.setAnswer3("answer3");
		request.setAnswer4("answer4");
		request.setAnswer5("answer5");
		request.setAnswer6("answer6");
		request.setAnswer7("answer7");
		request.setAnswer8("answer8");
		request.setAnswer9("answer9");
		request.setApprovalPhase("approval phase 1");
		request.setApprovalStatus("approved");
		request.setApprover("IBM Philippines");
		request.setGrandTotalCost(350_000.00);
		
		CapitalAttachment attach1 = new CapitalAttachment();
		attach1.setDpeManagerApproval(new byte[1024]);
		attach1.setName("attach 1");
		attach1.setRequest(request);
		attach1.setTemplate(new byte[1024]);
		
		CapitalAttachment attach2 = new CapitalAttachment();
		attach2.setDpeManagerApproval(new byte[1024]);
		attach2.setName("attach 2");
		attach2.setRequest(request);
		attach2.setTemplate(new byte[1024]);
		
		CapitalAttachment attach3 = new CapitalAttachment();
		attach3.setDpeManagerApproval(new byte[1024]);
		attach3.setName("attach 3");
		attach3.setRequest(request);
		attach3.setTemplate(new byte[1024]);
		
		request.getAttachments().add(attach1);
		request.getAttachments().add(attach2);
		request.getAttachments().add(attach3);
		
		CapitalEquipment equip1 = new CapitalEquipment();
		equip1.setRequest(request);
		equip1.setDescription("rubber seal");
		equip1.setQuantity(5);
		equip1.setUnitCost(10);
		equip1.setTotalCost(equip1.getComputedTotalCost());
		equip1.setType("car part");
		
		CapitalEquipment equip2 = new CapitalEquipment();
		equip2.setRequest(request);
		equip2.setDescription("windshield");
		equip2.setQuantity(2);
		equip2.setUnitCost(200);
		equip2.setTotalCost(equip2.getComputedTotalCost());
		equip2.setType("car part");
		
		CapitalEquipment equip3 = new CapitalEquipment();
		equip3.setRequest(request);
		equip3.setDescription("D15B");
		equip3.setQuantity(1);
		equip3.setUnitCost(1400);
		equip3.setTotalCost(equip3.getComputedTotalCost());
		equip3.setType("car part");
		
		request.getEquipments().add(equip1);
		request.getEquipments().add(equip2);
		request.getEquipments().add(equip3);
		
		session.save(request);
		
		session.getTransaction().commit();
		session.close();
	}

}
