package com.ibm.capitalapprovaltool.dao.impl;

import com.ibm.capitalapprovaltool.dao.DaoFactory;
import com.ibm.capitalapprovaltool.entity.CapitalAttachment;

public class CapitalAttachmentDaoFactoryImpl implements DaoFactory<CapitalAttachment> {

	@Override
	public CapitalAttachment buildDaoObject(CapitalAttachment attach) {

		attach.setDpeManagerApproval(new byte[1024]);
		attach.setName("attach 1");
		attach.setRequest(null);
		attach.setTemplate(new byte[1024]);
		
		return attach;
	}

}
