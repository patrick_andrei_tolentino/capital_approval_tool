package com.ibm.capitalapprovaltool.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ibm.capitalapprovaltool.dao.Dao;
import com.ibm.capitalapprovaltool.entity.User;
import com.ibm.capitalapprovaltool.util.HibernateFactoryUtil;

@Repository
@Transactional
public class UserDaoImpl implements Dao<User> {

	public boolean saveOrUpdate(User user) {
		HibernateFactoryUtil.getSession().saveOrUpdate(user);
		HibernateFactoryUtil.commitTransaction();
		return true;
	}
	
	public boolean save(User user) {
		HibernateFactoryUtil.getSession().save(user);
		HibernateFactoryUtil.commitTransaction();
		return true;
	}
	
	public boolean update(User user) {
		HibernateFactoryUtil.getSession().update(user);
		HibernateFactoryUtil.commitTransaction();
		return true;
	}


	@SuppressWarnings("unchecked")
	public List<User> getList() {
		return HibernateFactoryUtil.getSession().createCriteria(User.class).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
	}

	public boolean delete(User user) {
		try {
			HibernateFactoryUtil.getSession().delete(user);
			HibernateFactoryUtil.commitTransaction();
		} catch (Exception ex) {
			return false;
		}

		return true;
	}
}