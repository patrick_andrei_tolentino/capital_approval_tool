package com.ibm.capitalapprovaltool.dao.impl;

import java.util.Date;

import com.ibm.capitalapprovaltool.dao.DaoFactory;
import com.ibm.capitalapprovaltool.entity.CapitalRequest;
import com.ibm.capitalapprovaltool.enums.Organization;
import com.ibm.capitalapprovaltool.enums.QuarterYear;
import com.ibm.capitalapprovaltool.enums.RequestType;

public class CapitalRequestDaoFactoryImpl implements DaoFactory<CapitalRequest>{
	
	@Override
	public CapitalRequest buildDaoObject(CapitalRequest request) {
		
		request.setMasterDocNum("master doc 001");
		request.setType(RequestType.FACILITIES.getStringValue());
		request.setOrganization(Organization.GBS.toString());
		request.setCategory("sample category");
		request.setStatus("Pending for Approval");
		request.setDateCreated(new Date());
		request.setDateSubmitted(new Date());
		request.setContractName("Patrick's 5,000,000 USD contract");
		request.setProjectName("Capital Approval Process Tool");
		request.setSubmitter("Patrick Andrei V. Tolentino");
		request.setRequester("Christine Tayson");
		request.setForecasted(true);
		request.setQtrYearNeeded(QuarterYear.Q1_2018.getStringValue());
		request.setQtrYearAuth(QuarterYear.Q3_2018.getStringValue());
		request.setSiteLocation("Philippines");
		request.setAnswer1("answer1");
		request.setAnswer2("answer2");
		request.setAnswer3("answer3");
		request.setAnswer4("answer4");
		request.setAnswer5("answer5");
		request.setAnswer6("answer6");
		request.setAnswer7("answer7");
		request.setAnswer8("answer8");
		request.setAnswer9("answer9");
		request.setApprovalPhase("approval phase 1");
		request.setApprovalStatus("approved");
		request.setApprover("IBM Philippines");
		request.setGrandTotalCost(350_000.00);
		
		return request;
	}

}
