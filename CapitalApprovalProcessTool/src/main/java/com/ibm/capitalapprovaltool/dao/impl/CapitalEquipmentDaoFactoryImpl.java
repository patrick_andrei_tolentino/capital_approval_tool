package com.ibm.capitalapprovaltool.dao.impl;

import com.ibm.capitalapprovaltool.dao.DaoFactory;
import com.ibm.capitalapprovaltool.entity.CapitalEquipment;

public class CapitalEquipmentDaoFactoryImpl implements DaoFactory<CapitalEquipment>{

	@Override
	public CapitalEquipment buildDaoObject(CapitalEquipment equip) {
		
		equip.setRequest(null);
		equip.setDescription("rubber seal");
		equip.setQuantity(5);
		equip.setUnitCost(10);
		equip.setTotalCost(equip.getComputedTotalCost());
		equip.setType("car part");
		
		return equip;
	}
}
