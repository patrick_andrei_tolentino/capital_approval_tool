package com.ibm.capitalapprovaltool.dao;

public interface DaoFactory<T> {
	
	public T buildDaoObject(T t);
	
}
