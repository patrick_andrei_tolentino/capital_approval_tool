package com.ibm.capitalapprovaltool.dao;

import java.util.List;

public interface Dao<T> {
	
	public boolean saveOrUpdate(T t);

	public boolean save(T t);

	public boolean update(T t);

	public List<T> getList();

	public boolean delete(T t);
	
}
