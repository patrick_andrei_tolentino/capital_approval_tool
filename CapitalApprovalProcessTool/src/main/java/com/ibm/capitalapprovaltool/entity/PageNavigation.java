package com.ibm.capitalapprovaltool.entity;

public class PageNavigation {
	
	private String targetPage;
	private String previousPage;
	
	
	public String getTargetPage() {
		return targetPage;
	}
	public void setTargetPage(String targetPage) {
		this.targetPage = targetPage;
	}
	public String getPreviousPage() {
		return previousPage;
	}
	public void setPreviousPage(String previousPage) {
		this.previousPage = previousPage;
	}
	
}
