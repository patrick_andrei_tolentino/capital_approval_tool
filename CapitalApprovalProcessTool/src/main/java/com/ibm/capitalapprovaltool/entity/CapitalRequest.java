package com.ibm.capitalapprovaltool.entity;

import java.io.Serializable;
import java.util.*;

import javax.persistence.*;

@Entity
@Table(name = "capital_request")
public class CapitalRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4856827327871492687L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int requestId;

	@OneToMany(cascade = CascadeType.ALL, targetEntity=CapitalEquipment.class, mappedBy="request")
	private Collection<CapitalEquipment> equipments = new ArrayList<>();

	@OneToMany(cascade = CascadeType.ALL, targetEntity=CapitalAttachment.class, mappedBy="request")
	private Collection<CapitalAttachment> attachments = new ArrayList<>();

	@Column(name = "master_doc_num")
	private String masterDocNum;

	@Column(name = "request_type")
	private String type;

	@Column(name = "organization")
	private String organization;

	@Column(name = "category")
	private String category;

	@Column(name = "request_status")
	private String status;

	@Column(name = "date_created")
	@Temporal(TemporalType.DATE)
	private Date dateCreated;

	@Column(name = "date_submitted")
	@Temporal(TemporalType.DATE)
	private Date dateSubmitted;

	@Column(name = "contract_name")
	private String contractName;

	@Column(name = "project_name")
	private String projectName;

	@Column(name = "submitter_name")
	private String submitter;

	@Column(name = "requester_name")
	private String requester;
	
	@Column(name= "desc_capital_req")
	private String descriptionOfCapitalReq;

	@Column(name = "is_forecasted", columnDefinition="bit")
	private boolean isForecasted;

	@Column(name = "qtr_year_needed")
	private String qtrYearNeeded;

	@Column(name = "qtr_year_auth")
	private String qtrYearAuth;

	@Column(name = "site_location")
	private String siteLocation;

	@Column(name = "just_answer_1")
	private String answer1;

	@Column(name = "just_answer_2")
	private String answer2;

	@Column(name = "just_answer_3")
	private String answer3;

	@Column(name = "just_answer_4")
	private String answer4;

	@Column(name = "just_answer_5")
	private String answer5;

	@Column(name = "just_answer_6")
	private String answer6;

	@Column(name = "just_answer_7")
	private String answer7;

	@Column(name = "just_answer_8")
	private String answer8;

	@Column(name = "just_answer_9")
	private String answer9;

	@Column(name = "approval_phase")
	private String approvalPhase;

	@Column(name = "approval_status")
	private String approvalStatus;

	@Column(name = "approver")
	private String approver;

	@Column(name = "grand_total_cost")
	private double grandTotalCost;

	public int getRequestId() {
		return requestId;
	}

	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

	public Collection<CapitalEquipment> getEquipments() {
		return equipments;
	}

	public void setEquipments(Collection<CapitalEquipment> equipments) {
		this.equipments = equipments;
	}

	public Collection<CapitalAttachment> getAttachments() {
		return attachments;
	}

	public void setAttachments(Collection<CapitalAttachment> attachments) {
		this.attachments = attachments;
	}

	public String getMasterDocNum() {
		return masterDocNum;
	}

	public void setMasterDocNum(String masterDocNum) {
		this.masterDocNum = masterDocNum;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateSubmitted() {
		return dateSubmitted;
	}

	public void setDateSubmitted(Date dateSubmitted) {
		this.dateSubmitted = dateSubmitted;
	}

	public String getContractName() {
		return contractName;
	}

	public void setContractName(String contractName) {
		this.contractName = contractName;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getSubmitter() {
		return submitter;
	}

	public void setSubmitter(String submitter) {
		this.submitter = submitter;
	}

	public String getRequester() {
		return requester;
	}

	public void setRequester(String requester) {
		this.requester = requester;
	}

	public String getDescriptionOfCapitalReq() {
		return descriptionOfCapitalReq;
	}

	public void setDescriptionOfCapitalReq(String descriptionOfCapitalReq) {
		this.descriptionOfCapitalReq = descriptionOfCapitalReq;
	}

	public boolean isForecasted() {
		return isForecasted;
	}

	public void setForecasted(boolean isForecasted) {
		this.isForecasted = isForecasted;
	}

	public String getQtrYearNeeded() {
		return qtrYearNeeded;
	}

	public void setQtrYearNeeded(String qtrYearNeeded) {
		this.qtrYearNeeded = qtrYearNeeded;
	}

	public String getQtrYearAuth() {
		return qtrYearAuth;
	}

	public void setQtrYearAuth(String qtrYearAuth) {
		this.qtrYearAuth = qtrYearAuth;
	}

	public String getSiteLocation() {
		return siteLocation;
	}

	public void setSiteLocation(String siteLocation) {
		this.siteLocation = siteLocation;
	}

	public String getAnswer1() {
		return answer1;
	}

	public void setAnswer1(String answer1) {
		this.answer1 = answer1;
	}

	public String getAnswer2() {
		return answer2;
	}

	public void setAnswer2(String answer2) {
		this.answer2 = answer2;
	}

	public String getAnswer3() {
		return answer3;
	}

	public void setAnswer3(String answer3) {
		this.answer3 = answer3;
	}

	public String getAnswer4() {
		return answer4;
	}

	public void setAnswer4(String answer4) {
		this.answer4 = answer4;
	}

	public String getAnswer5() {
		return answer5;
	}

	public void setAnswer5(String answer5) {
		this.answer5 = answer5;
	}

	public String getAnswer6() {
		return answer6;
	}

	public void setAnswer6(String answer6) {
		this.answer6 = answer6;
	}

	public String getAnswer7() {
		return answer7;
	}

	public void setAnswer7(String answer7) {
		this.answer7 = answer7;
	}

	public String getAnswer8() {
		return answer8;
	}

	public void setAnswer8(String answer8) {
		this.answer8 = answer8;
	}

	public String getAnswer9() {
		return answer9;
	}

	public void setAnswer9(String answer9) {
		this.answer9 = answer9;
	}

	public String getApprovalPhase() {
		return approvalPhase;
	}

	public void setApprovalPhase(String approvalPhase) {
		this.approvalPhase = approvalPhase;
	}

	public String getApprovalStatus() {
		return approvalStatus;
	}

	public void setApprovalStatus(String approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	public String getApprover() {
		return approver;
	}

	public void setApprover(String approver) {
		this.approver = approver;
	}

	public double getGrandTotalCost() {
		return grandTotalCost;
	}

	public void setGrandTotalCost(double grandTotalCost) {
		this.grandTotalCost = grandTotalCost;
	}

}
