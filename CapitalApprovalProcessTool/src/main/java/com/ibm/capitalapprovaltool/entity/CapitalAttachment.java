package com.ibm.capitalapprovaltool.entity;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "capital_attachment")
public class CapitalAttachment implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7558231498497427939L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id")
	private int attachmentId;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name="request_id")
	private CapitalRequest request;

	@Column(name="name")
	private String name;
	
	@Column(name="capital_appr_template", columnDefinition="blob")
	private byte[] template;
	
	@Column(name="dpe_mgr_appr", columnDefinition="blob")
	private byte[] dpeManagerApproval;
	
	
	public int getAttachmentId() {
		return attachmentId;
	}
	public void setAttachmentId(int attachmentId) {
		this.attachmentId = attachmentId;
	}
	public CapitalRequest getRSequest() {
		return request;
	}
	public void setRequest(CapitalRequest request) {
		this.request = request;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public byte[] getTemplate() {
		return template;
	}
	public void setTemplate(byte[] template) {
		this.template = template;
	}
	public byte[] getDpeManagerApproval() {
		return dpeManagerApproval;
	}
	public void setDpeManagerApproval(byte[] dpeManagerApproval) {
		this.dpeManagerApproval = dpeManagerApproval;
	}
	
}
