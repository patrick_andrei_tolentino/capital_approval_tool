package com.ibm.capitalapprovaltool.constants;

public class ViewConstants {
	public static final String NEW_REQUEST_VIEW = "Create New Request";
	public static final String REQUEST_HISTORY_VIEW = "View Request History";
	public static final String ADMIN_VIEW = "View Administration";
}
