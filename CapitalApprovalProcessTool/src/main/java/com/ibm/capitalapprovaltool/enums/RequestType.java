package com.ibm.capitalapprovaltool.enums;

public enum RequestType {
	ALL_OTHER("All Other"), FACILITIES("Facilities"), OTHER_IT("Other IT"), SYS_SUPPORT("System Support"),
	WORKSTATION("Workstation"), SOD("SOD");
	
	private String stringValue;
	
	private RequestType(String stringValue) {
		this.stringValue = stringValue;
	}
	
	public String getStringValue() {
		return this.stringValue;
	}
}
