package com.ibm.capitalapprovaltool.enums;

public enum CapitalCategories {
	BUILD("Build Capital"), CONTRACT("Contract Unique Capital"), PROJECT("Project Unique Capital"), 
	RETROFIT("Retrofit Facility & Infrastructure");
	
	private String stringValue;
	
	private CapitalCategories(String stringValue) {
		this.stringValue = stringValue;
	}
	
	public String getStringValue() {
		return this.stringValue;
	}
}
