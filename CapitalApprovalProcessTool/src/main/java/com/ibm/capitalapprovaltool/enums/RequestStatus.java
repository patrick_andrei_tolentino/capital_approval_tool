package com.ibm.capitalapprovaltool.enums;

public enum RequestStatus {
	DRAFT("Draft"), SUBMITTED("Submitted"), AWAIT1("Awaiting Approver 1"), AWAIT2("Awaiting Approver 2"), 
	AWAIT3("Awaiting Approver 3"), APPROVED("Approved");
	
private String stringValue;
	
	private RequestStatus(String stringValue) {
		this.stringValue = stringValue;
	}
	
	public String getStringValue() {
		return this.stringValue;
	}
}
