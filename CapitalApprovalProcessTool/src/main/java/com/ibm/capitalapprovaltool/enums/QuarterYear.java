package com.ibm.capitalapprovaltool.enums;

public enum QuarterYear {
	Q1_2018("Q1 2018"), Q2_2018("Q2 2018"), Q3_2018("Q3 2018"), Q4_2018("Q41 2018");
	
	private String stringValue;
	
	private QuarterYear(String stringValue) {
		this.stringValue = stringValue;
	}
	
	public String getStringValue() {
		return this.stringValue;
	}
}
