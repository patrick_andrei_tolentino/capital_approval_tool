package com.ibm.capitalapprovaltool.controller;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ibm.capitalapprovaltool.entity.User;

@Controller
public class CapitalRequestController {

	@RequestMapping("capitalrequest")
	public ModelAndView getCapitalRequestDetails(@Valid @ModelAttribute("user") User user, BindingResult result) {
		
		if(result.hasErrors()) {
			result.getAllErrors().stream().forEach(System.out::println);
			return new ModelAndView("/users");
		}
		
		ModelAndView mv = new ModelAndView("capitalrequest");
		mv.addObject("msg", "Hello World " + "My email name is " + user.getEmail());
				
		return mv;
	}
	
	@ModelAttribute
	public void addHeaderToModel(Model model) {
		model.addAttribute("headerMessage", "Header Message");
	}
}
