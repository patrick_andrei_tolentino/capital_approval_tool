package com.ibm.capitalapprovaltool.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.ibm.capitalapprovaltool.dao.Dao;
import com.ibm.capitalapprovaltool.entity.User;

@Controller
public class LoginController {

	@Autowired
    Dao<User> userDao;
	
	@RequestMapping(value = "login", method = RequestMethod.GET)
    public ModelAndView getPage(@Valid @ModelAttribute("user") User user, BindingResult result) {
		
		if(result.hasErrors()) {
			return new ModelAndView("login");
		}
		
        ModelAndView view = new ModelAndView("mainmenu");
        view.addObject("welcomeMsg", "Welcome to Capital Request Tool " + user.getFirstName());
        return view;
    }
}
