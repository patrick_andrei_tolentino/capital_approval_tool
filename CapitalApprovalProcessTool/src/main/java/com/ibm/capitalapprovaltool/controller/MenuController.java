package com.ibm.capitalapprovaltool.controller;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.ibm.capitalapprovaltool.constants.ViewConstants;
import com.ibm.capitalapprovaltool.entity.CapitalRequest;
import com.ibm.capitalapprovaltool.entity.PageNavigation;

@Controller
public class MenuController {
	
	@RequestMapping(value = "mainmenu", method = RequestMethod.POST)
	public ModelAndView getTargetPage(@Valid @ModelAttribute("page") PageNavigation viewPage, BindingResult result) {
		
		if(viewPage.getTargetPage().equals(ViewConstants.NEW_REQUEST_VIEW)) {
			return new ModelAndView("createrequest");
		}
		
		if(viewPage.getTargetPage().equals(ViewConstants.REQUEST_HISTORY_VIEW)) {
			return new ModelAndView("users");
		}
		
		if(viewPage.getTargetPage().equals(ViewConstants.ADMIN_VIEW)) {
			return new ModelAndView("admin");
		}
		
		return new ModelAndView("mainmenu");
	}
	
	
	
}
